<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.aedev V0.3.24 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# tpl_app 0.3.12

[![GitLab develop](https://img.shields.io/gitlab/pipeline/aedev-group/aedev_tpl_app/develop?logo=python)](
    https://gitlab.com/aedev-group/aedev_tpl_app)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/aedev-group/aedev_tpl_app/release0.3.11?logo=python)](
    https://gitlab.com/aedev-group/aedev_tpl_app/-/tree/release0.3.11)
[![PyPIVersions](https://img.shields.io/pypi/v/aedev_tpl_app)](
    https://pypi.org/project/aedev-tpl-app/#history)

>aedev_tpl_app package 0.3.12.

[![Coverage](https://aedev-group.gitlab.io/aedev_tpl_app/coverage.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_app/coverage/index.html)
[![MyPyPrecision](https://aedev-group.gitlab.io/aedev_tpl_app/mypy.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_app/lineprecision.txt)
[![PyLintScore](https://aedev-group.gitlab.io/aedev_tpl_app/pylint.svg)](
    https://aedev-group.gitlab.io/aedev_tpl_app/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/aedev_tpl_app)](
    https://gitlab.com/aedev-group/aedev_tpl_app/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/aedev_tpl_app)](
    https://gitlab.com/aedev-group/aedev_tpl_app/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/aedev_tpl_app)](
    https://gitlab.com/aedev-group/aedev_tpl_app/)
[![PyPIFormat](https://img.shields.io/pypi/format/aedev_tpl_app)](
    https://pypi.org/project/aedev-tpl-app/)
[![PyPILicense](https://img.shields.io/pypi/l/aedev_tpl_app)](
    https://gitlab.com/aedev-group/aedev_tpl_app/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/aedev_tpl_app)](
    https://libraries.io/pypi/aedev-tpl-app)
[![PyPIDownloads](https://img.shields.io/pypi/dm/aedev_tpl_app)](
    https://pypi.org/project/aedev-tpl-app/#files)


## installation


execute the following command to install the
aedev.tpl_app package
in the currently active virtual environment:
 
```shell script
pip install aedev-tpl-app
```

if you want to contribute to this portion then first fork
[the aedev_tpl_app repository at GitLab](
https://gitlab.com/aedev-group/aedev_tpl_app "aedev.tpl_app code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(aedev_tpl_app):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/aedev-group/aedev_tpl_app/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://aedev.readthedocs.io/en/latest/_autosummary/aedev.tpl_app.html
"aedev_tpl_app documentation").
